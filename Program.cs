using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SapApiOasis
{
    public class Program
    {
        public static string ErrorPathXml { get; set; }
        public static string TempPathXml { get; set; }
        public static double IVA { get; set; }

        public static void Main(string[] args)
        {
            ValidarCarpetas();
            CreateHostBuilder(args).Build().Run();            
        }

        public static void ValidarCarpetas()
        {
            ErrorPathXml = System.IO.Directory.GetCurrentDirectory() + @"\XmlError\";

            // Valida si existe el directorio de XML con errores.
            if (!System.IO.Directory.Exists(ErrorPathXml))
            {
                // Crea el directorio de errores.
                System.IO.Directory.CreateDirectory(ErrorPathXml);
            }

            TempPathXml = System.IO.Directory.GetCurrentDirectory() + @"\TmpXml\";
            // Valida si existe el directorio de XML temporales.
            if (!System.IO.Directory.Exists(TempPathXml))
            {
                // Crea el directorio de temporales.
                System.IO.Directory.CreateDirectory(TempPathXml);
            }
        }

        /// <summary>
        /// Almacena la estructura en la carpeta de errores.
        /// </summary>
        /// <param name="Xml">
        /// Contenido XML que se va almacenar.
        /// </param>
        public static void GuardarErrorXml(string Tipo, string Xml)
        {            
            try
            {
                string XmlPath = Program.ErrorPathXml + Tipo + "-" + System.Guid.NewGuid() + ".xml";
                System.IO.File.WriteAllText(XmlPath, Xml);
            }
            catch (Exception e)
            {

            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
