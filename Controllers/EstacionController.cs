﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SoapHttpClient;
using SoapHttpClient.Enums;
using System.Net;
using System.Net.Http;
using System.Xml.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace SapApiOasis.Controllers
{
    [Route("administracion/estacion")]
    [Authorize]
    [ApiController]
    public class EstacionController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public EstacionController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpGet]
        public Models.Estacion Get(string codigo_sap)
        {
            Models.Estacion estacion = new Models.Estacion();

            if (codigo_sap == null)
            {
                estacion.errorCode = "1";
                estacion.errorMsg = "El código de estación no puede ser nulo o vacío.";
            }
            else
            {
                string error;
                estacion = ObtenerEstacion(codigo_sap, out error);
                estacion.errorCode = "0";
                estacion.errorMsg = string.Empty;

                if (error != string.Empty)
                {
                    estacion.errorCode = "2";
                    estacion.errorMsg = error;
                }
            }

            return estacion;
        }

        private Models.Estacion ObtenerEstacion(string codigo_sap, out string error)
        {
            error = string.Empty;
            Models.Estacion estacion = new Models.Estacion();
            Models.DataBase dataBase = new Models.DataBase();

            if (!dataBase.Conectar(this.configuration["B1iConfig:SqlSap"]))
            {
                error = "No se pudo conectar a la base de datos.";
                return estacion;
            }
            else
            {
                System.Data.DataSet infoEstacion = dataBase.ValidarCodigoEstacion(codigo_sap, out error);
                dataBase.Desconectar();
                if (error == string.Empty)
                {
                    if (infoEstacion.Tables[0].Rows.Count > 0)
                    {
                        System.Data.DataRow dataEds = infoEstacion.Tables[0].Rows[0];
                        string Eds = dataEds["NombreEDS"].ToString();
                        Models.EstacionResponse estacionResponse = new Models.EstacionResponse(
                            dataEds["NombreEDS"].ToString(), dataEds["Zona"].ToString(),
                            dataEds["Plaza"].ToString(), dataEds["Calle"].ToString(),
                            dataEds["Numero"].ToString(), dataEds["Colonia"].ToString(),
                            dataEds["Ciudad"].ToString(), dataEds["CodigoPostal"].ToString());
                        estacion.response = estacionResponse;
                    }
                    else
                    {
                        error = "No se encontró la estación de servicio para el código: " + codigo_sap;
                    }
                }
            }

            return estacion;
        }

    }
}
