﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SoapHttpClient;
using SoapHttpClient.Enums;
using Microsoft.AspNetCore.Authorization;

namespace SapApiOasis.Controllers
{
    [Route("administracion/orden-venta/enviar")]
    [Authorize]
    [ApiController]
    public class OrdenVentaController : ControllerBase
    {
        private readonly IConfiguration configuration;
        public OrdenVentaController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpPost]
        public async Task<Models.OrdenVentaResponse> Post(Models.OrdenVenta[] ordenesVenta)
        {
            Models.OrdenVentaResponse response = new Models.OrdenVentaResponse();
            response.errorCode = 0;
            response.errorMsg = "";

            // Conecta a la base de datos SQL.
            Models.DataBase dataBase = new Models.DataBase();
            if (!dataBase.Conectar(this.configuration["B1iConfig:SqlSap"]))
            {
                response.errorCode = 2;
                response.errorMsg = "No se pudo conectar a la base de datos.";
                return response;
            }

            // Prepara la respuesta de los documentos.
            response.response = new List<Models.OrdenVentaResponse.Response>(); 
            Models.OrdenVentaResponse.Response responseItem = new Models.OrdenVentaResponse.Response();

            // Recorre cada Orden de venta recibida
            foreach (Models.OrdenVenta ordenVenta in ordenesVenta)
            {
                try
                {
                    // Obtiene la información complementaria de la EDS.
                    var infoEstacion = await dataBase.ValidarOrdenVenta(ordenVenta.codigoSAPEDS);

                    // Valida si se recibió un error de la BD.
                    if (infoEstacion.GetType() == typeof(string))
                    {
                        response.errorCode = 1;
                        response.errorMsg += "Error Ticket " + ordenVenta.numeroTicket + ": " + infoEstacion.ToString() + ". ";
                        responseItem.folioSAP = 0;
                        responseItem.numeroDocumento = 0;
                        responseItem.numeroTicket = ordenVenta.numeroTicket;
                        response.response.Add(responseItem);
                    }
                    else
                    {
                        // Valida que se haya obtenido algún registro.
                        if (((System.Data.DataSet)infoEstacion).Tables[0].Rows.Count > 0)
                        {
                            // Llena la información complementaria de la EDS.
                            System.Data.DataRow dataEds = ((System.Data.DataSet)infoEstacion).Tables[0].Rows[0];
                            ordenVenta.ciudad = dataEds["Ciudad"].ToString();
                            ordenVenta.region = dataEds["Region"].ToString();
                            ordenVenta.proyecto = dataEds["Proyecto"].ToString();
                            ordenVenta.almacen = dataEds["Almacen"].ToString();
                            ordenVenta.permisoCRE = dataEds["PermisoCRE"].ToString();

                            // Genera la estructura XML para enviar a SAP.
                            // Obtiene la estructura XML-SAP para la Salida de Mercancía.
                            
                            // Client SOAP para conectar a B1IF.
                            SoapClient soapClient = new SoapClient();
                            XNamespace nameSpace = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                            var resultSoap =
                                await soapClient.PostAsync(
                                    new Uri(this.configuration["B1iConfig:UrlCrearSalidaMercancias"]),
                                    SoapVersion.Soap11,
                                    body: XElement.Parse(ordenVenta.XmlSAP()));
                            
                            // Lee la respuesta SOAP de B1IF.
                            XDocument doc = XDocument.Parse(resultSoap.Content.ReadAsStringAsync().Result);
                            IEnumerable<XElement> responses = doc.Descendants("response");
                            string result = string.Empty;
                            string actionMessage = string.Empty;
                            foreach (XElement res in responses)
                            {
                                result = (string)res.Element("Result");
                                actionMessage = (string)res.Element("ActionMessage");
                            }

                            // Valida si se creo la Oferta de Venta.
                            if (result == "success")
                            {
                                responseItem.folioSAP = int.Parse(actionMessage);
                                responseItem.numeroDocumento = int.Parse(actionMessage);
                                responseItem.numeroTicket = ordenVenta.numeroTicket;
                                response.response.Add(responseItem);
                            }
                            // Devuelve el mensaje de error.
                            else
                            {
                                response.errorCode = 4;
                                response.errorMsg += "Error ticket " + ordenVenta.numeroTicket + ": " + actionMessage;
                                responseItem.folioSAP = 0;
                                responseItem.numeroDocumento = 0;
                                responseItem.numeroTicket = ordenVenta.numeroTicket;
                                response.response.Add(responseItem);
                            }
                        }
                        else
                        {
                            response.errorCode = 1;
                            response.errorMsg += "Error ticket " + ordenVenta.numeroTicket + ": " +  "Revise la confirguación de la EDS en SAP.";
                            responseItem.folioSAP = 0;
                            responseItem.numeroDocumento = 0;
                            responseItem.numeroTicket = ordenVenta.numeroTicket;
                            response.response.Add(responseItem);
                            Program.GuardarErrorXml("OfV", ordenVenta.XmlSAP());
                        }
                    }
                }
                catch (Exception e)
                {
                    response.errorCode = 3;
                    response.errorMsg = e.Message;
                    Program.GuardarErrorXml("OfV", ordenVenta.XmlSAP());
                }
            }
            
            dataBase.Desconectar();
            
            // Devuelve la respuesta.
            return response;
        }
    }
}
