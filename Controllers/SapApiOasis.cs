﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;

namespace SapApiOasis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SapApiOasis : ControllerBase
    {
        private static readonly HttpClient client = new HttpClient();

        [HttpGet]
        public string Get()
        {
            return "Hola";
        }
    }
}
