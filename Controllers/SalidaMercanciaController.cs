﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using SoapHttpClient;
using SoapHttpClient.Enums;

namespace SapApiOasis.Controllers
{
    [Route("administracion/mercancia/salida")]
    [Authorize]
    [ApiController]
    public class SalidaMercanciaController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public SalidaMercanciaController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpPost]
        public async Task<Models.SalidaMercanciaResponse> Post(Models.SalidaMercancia salidaMercancia)
        {
            Models.SalidaMercanciaResponse response = new Models.SalidaMercanciaResponse();

            try
            {
                if (salidaMercancia.productos.Length > 0)
                {
                    //    // Consulta el almacén y centro de costos de la estación.
                    Models.DataBase dataBase = new Models.DataBase();
                    dataBase.Conectar(this.configuration["B1iConfig:SqlSap"]);
                    string errorDataBase = null;

                    for (int i = 0; i < salidaMercancia.productos.Length; i++)
                    {
                        dataBase.ValidaConfirmacionEnvio(salidaMercancia.numeroEnvio, ref salidaMercancia.productos[i], out errorDataBase);
                        if (errorDataBase != "OK")
                        {
                            response.errorCode = 3;
                            response.errorMsg = errorDataBase;

                            return response;
                        }
                    }

                    response.errorCode = 0;
                    response.errorMsg = "Confirmación existosa.";
                }
                else
                {
                    response.errorCode = 2;
                    response.errorMsg = "Debe agregar al menos un producto.";
                }
            }
            catch (Exception e)
            {
                response.errorCode = 1;
                response.errorMsg = e.Message;
            }
            
            return response;
        }        
    }
}
