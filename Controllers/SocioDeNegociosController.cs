﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace SapApiOasis.Controllers
{
    [Route("administracion/cliente")]
    [Authorize]
    [ApiController]
    public class SocioDeNegociosController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public SocioDeNegociosController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpGet]
        public Models.SocioDeNegocios Get(string codigo_sap)
        {
            Models.SocioDeNegocios socioNegocios = new Models.SocioDeNegocios();

            if (codigo_sap == null)
            {
                socioNegocios.errorCode = "1";
                socioNegocios.errorMsg = "El código de cliente no puede ser nulo o vacío.";
            }
            else
            {
                string error;
                socioNegocios = this.ObtenerSocioDeNegocios(codigo_sap, out error);
                socioNegocios.errorCode = "0";
                socioNegocios.errorMsg = string.Empty;

                if (error != string.Empty)
                {
                    socioNegocios.errorCode = "2";
                    socioNegocios.errorMsg = error;
                }
            }

            return socioNegocios;
        }

        public Models.SocioDeNegocios ObtenerSocioDeNegocios(string codigo_sap, out string error)
        {
            error = string.Empty;
            Models.SocioDeNegocios socioNegocios = new Models.SocioDeNegocios();

            Models.DataBase dataBase = new Models.DataBase();
            if (!dataBase.Conectar(this.configuration["B1iConfig:SqlSap"]))
            {
                error = "No se pudo conectar a la base de datos.";
                return socioNegocios;
            }
            else
            {
                System.Data.DataSet infoCliente = dataBase.ValidarSocioDeNegocios(codigo_sap, out error);
                dataBase.Desconectar();
                if (error == string.Empty)
                {
                    Models.SocioDeNegociosResponse socioNegociosResponse = new Models.SocioDeNegociosResponse();

                    if (infoCliente.Tables[0].Rows.Count > 0)
                    {
                        System.Data.DataRow dataSN = infoCliente.Tables[0].Rows[0];
                        socioNegociosResponse.existeCliente = "true";
                        socioNegociosResponse.limiteCredito = double.Parse(dataSN["CreditLine"].ToString());
                    }
                    else
                    {
                        socioNegociosResponse.existeCliente = "false";
                        socioNegociosResponse.limiteCredito = 0;
                    }
                    socioNegocios.response = socioNegociosResponse;
                }
            }

            return socioNegocios;
        }
    }
}
