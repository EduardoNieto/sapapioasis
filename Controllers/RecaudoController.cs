﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{

    [Route("administracion/recaudos/")]
    [Authorize]
    [ApiController]
    public class RecaudoController : Controller
    {
        private readonly IConfiguration configuration;

        public RecaudoController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpPost]
        public async Task<Models.RecaudoResponse> Post(Models.Recaudo recaudos)
        {
            Models.RecaudoResponse response = new RecaudoResponse();

            if (recaudos.codigoSAP == string.Empty)
            {
                response.errorCode = 1;
                response.errorMsg = "El código de la EDS no puede estar vacío.";
            }
            else if (recaudos.fechaCierreDia == string.Empty)
            {
                response.errorCode = 2;
                response.errorMsg = "La fecha no puede estar vacía";
            }
            else if (recaudos.financieras == null)
            { 
                response.errorCode = 3;
                response.errorMsg = "Por lo menos debe enviar una financiera.";                
            }
            else
            {
                response.errorCode = 0;
                response.errorMsg = "";
                response.response = new List<RecaudoResponse.Response>();

                foreach (Models.Financiera financiera in recaudos.financieras)
                {
                    Models.RecaudoResponse.Response respFinanciera = new RecaudoResponse.Response();
                    respFinanciera.nombre = financiera.nombre;
                    respFinanciera.status = "OK";
                    response.response.Add(respFinanciera);
                }
            }

            return response;            
        }
    }
}