﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace SapApiOasis.Controllers
{
    [Route("administracion/producto")]
    [Authorize]
    [ApiController]
    public class ArticuloController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public ArticuloController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpGet]
        public async Task<Models.Articulo> Get(string codigo_sap)
        {
            Models.Articulo articulo = new Models.Articulo();

            if (codigo_sap == null)
            {
                articulo.errorCode = "1";
                articulo.errorMsg = "El código de artículo no puede ser nulo o vacío.";
            }
            else
            {
                articulo = await this.ObtenerArticulo(codigo_sap);
            }

            return articulo;
        }

        public async Task<Models.Articulo> ObtenerArticulo(string codigo_sap)
        {
            Models.Articulo articulo = new Models.Articulo(); 

            Models.DataBase dataBase = new Models.DataBase();
            if (!dataBase.Conectar(this.configuration["B1iConfig:SqlSap"]))
            {
                articulo.errorCode = "2";
                articulo.errorMsg = "No se pudo conectar a la base de datos.";
                return articulo;
            }
            else
            {
                var infoArticulo = await dataBase.ValidarArticulo(codigo_sap);
                //System.Data.DataSet infoArticulo = 
                dataBase.Desconectar();
                if (infoArticulo.GetType() == typeof(string))
                {
                    articulo.errorCode = "3";
                    articulo.errorMsg = infoArticulo.ToString();
                }
                else
                {
                    Models.ArticuloResponse articuloResponse = new Models.ArticuloResponse();
                    if (((System.Data.DataSet)infoArticulo).Tables[0].Rows.Count > 0)
                    {
                        articulo.errorCode = "0";
                        articulo.errorMsg = string.Empty;

                        System.Data.DataRow dataArticulo = ((System.Data.DataSet)infoArticulo).Tables[0].Rows[0];
                        articuloResponse.descripcion = dataArticulo["ItemName"].ToString();
                        articuloResponse.codigoUnidadMedida = dataArticulo["SalUnitMsr"].ToString();
                        articuloResponse.unidadMedida = dataArticulo["SalPackMsr"].ToString();
                        articuloResponse.iva = int.Parse(dataArticulo["Iva"].ToString());
                        articuloResponse.precio = double.Parse(dataArticulo["Price"].ToString());
                        articuloResponse.stockMinimo = double.Parse(dataArticulo["MinLevel"].ToString());

                        articulo.response = articuloResponse;
                    }
                    else
                    {
                        articulo.errorCode = "4";
                        articulo.errorMsg = $@"No existe el artículo {codigo_sap}";
                    }                    
                }
            }

            return articulo;
        }
    }
}
