﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Controllers
{
    [Route("administracion/montos_tpv/")]
    [Authorize]
    [ApiController]
    public class MontoTPVController : Controller
    {
        private readonly IConfiguration configuration;

        public MontoTPVController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpPost]
        public async Task<Models.MontosTPVResponse> Post(Models.MontoTPV monto)
        {
            Models.MontosTPVResponse response = new Models.MontosTPVResponse();

            if (monto.codigoSAP == string.Empty || monto.codigoSAP == null)
            {
                response.errorCode = 1;
                response.errorMsg = "El código de la EDS no puede estar vacío.";
            }            
            else
            {
                // Conecta a la base de datos SQL.
                Models.DataBase dataBase = new Models.DataBase();
                if (!dataBase.Conectar(this.configuration["B1iConfig:SqlSap"]))
                {
                    response.errorCode = 2;
                    response.errorMsg = "No se pudo conectar a la base de datos.";
                    return response;
                }

                // Obtiene la información complementaria de la EDS.
                var infoEstacion = await dataBase.ValidarOrdenVenta(monto.codigoSAP);

                // Valida si se recibió un error de la BD.
                if (infoEstacion.GetType() == typeof(string))
                {
                    response.errorCode = 1;
                    response.errorMsg = "Error SAP: " + infoEstacion.ToString();
                }
                else if (monto.fechaCierreDia == string.Empty || monto.fechaCierreDia == null)
                {
                    response.errorCode = 2;
                    response.errorMsg = "La fecha no puede estar vacía";
                }
                else if (monto.montoTarjetaCredito < 0)
                {
                    response.errorCode = 3;
                    response.errorMsg = "El monto de Tarjeta de Crédito no puede ser menor a cero.";
                }
                else if (monto.montoTarjetaDebito < 0)
                {
                    response.errorCode = 4;
                    response.errorMsg = "El monto de Tarjeta de Débito no puede ser menor a cero.";
                }
                else
                {
                    response.errorCode = 0;
                    response.errorMsg = "OK";
                }
            }

            return response;
        }
    }
}
