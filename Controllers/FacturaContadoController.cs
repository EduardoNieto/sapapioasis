﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SoapHttpClient;
using SoapHttpClient.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SapApiOasis.Controllers
{
    [Route("administracion/factura/contado/")]
    [Authorize]
    [ApiController]
    public class FacturaContadoController : Controller
    {
        private readonly IConfiguration configuration;

        public FacturaContadoController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpPost]
        public async Task<Models.FacturaContadoResponse> Post(List<Models.FacturaContado> facturas)
        {
            Models.FacturaContadoResponse response = new Models.FacturaContadoResponse();
            response.errorCode = 0;
            response.errorMsg = "";
            XNamespace nsCfdi = "http://www.sat.gob.mx/cfd/3";
            XNamespace nsTfd = "http://www.sat.gob.mx/TimbreFiscalDigital";

            // Conecta a la base de datos SQL.
            Models.DataBase dataBase = new Models.DataBase();
            if (!dataBase.Conectar(this.configuration["B1iConfig:SqlSap"]))
            {
                response.errorCode = 2;
                response.errorMsg = "No se pudo conectar a la base de datos.";
                return response;
            }

            // Prepara la respuesta de los documentos.
            response.response = new List<Models.FacturaContadoResponse.Response>();
            Models.FacturaContadoResponse.Response responseItem = new Models.FacturaContadoResponse.Response();

            // Procesa cada factura recibida.
            foreach (Models.FacturaContado factura in facturas)
            {
                string uuid = string.Empty;

                try
                {
                    // Decodifica el XML en Base 64.
                    string xmlDecode = Base64Decode(factura.xml);
                    // Convierte el string en un XML Document.
                    XDocument xDocument = XDocument.Parse(xmlDecode);
                    // Carga el nodo Complemento
                    var complemento = from c in xDocument.Descendants(nsTfd + "TimbreFiscalDigital") select c;
                    // Lee el UUID del XML.
                    foreach (var c in complemento)
                    {
                        uuid = c.Attribute("UUID").Value;
                    }

                    // Obtiene la información complementaria de la EDS.
                    var infoEstacion = await dataBase.ValidarOrdenVenta(factura.codigoSAPEDS);

                    // Valida si se recibió un error de la BD.
                    if (infoEstacion.GetType() == typeof(string))
                    {
                        response.errorCode = 1;
                        response.errorMsg += "Error Factura " + uuid + ": " + infoEstacion.ToString() + ". ";
                        responseItem.numeroDocumento = 0;
                        responseItem.uuid = uuid;
                        response.response.Add(responseItem);
                    }
                    else
                    {
                        // Valida que se haya obtenido algún registro.
                        if (((System.Data.DataSet)infoEstacion).Tables[0].Rows.Count > 0)
                        {
                            // Carga los datos generales del Comprobante.
                            var comprobante = from c in xDocument.Descendants(nsCfdi + "Comprobante") select c;
                            // Lee los datos del XML.
                            foreach (var c in comprobante)
                            {
                                factura.fechaVenta = c.Attribute("Fecha").Value;
                                factura.fechaVenta = factura.fechaVenta.Substring(0, 4) + factura.fechaVenta.Substring(5, 2)
                                    + factura.fechaVenta.Substring(8, 2);
                                factura.hora = c.Attribute("Fecha").Value;
                                factura.hora = factura.hora.Substring(11, 2) + factura.hora.Substring(14, 2);

                                factura.formaPago = c.Attribute("FormaPago").Value;
                                // Concatena el tipo de consumo y la cuenta contable.
                                if (factura.formaPago == "01")
                                    factura.medioPago = "E";
                                else if (factura.formaPago == "04")
                                    factura.medioPago = "C";
                                else if (factura.formaPago == "28")
                                    factura.medioPago = "D";
                                else if (factura.formaPago == "99")
                                    factura.medioPago = "P";
                            }

                            // Código cliente SAP de Público en General.
                            factura.codigoSAPCliente = "CNQU120510QZ7";
                            factura.referencia = "OASIS";
                            factura.comentario = "Interfaz OASIS Factura Contado";

                            System.Data.DataRow dataEds = ((System.Data.DataSet)infoEstacion).Tables[0].Rows[0];

                            factura.detalles = new List<Models.FacturaDetalle>();
                            // Lee el contenido del Concepto y lo pasa al Detalle de la factura
                            var conceptos = from c in xDocument.Descendants(nsCfdi + "Concepto")
                                            select c;
                            foreach (var c in conceptos)
                            {
                                Models.FacturaDetalle detalle = new Models.FacturaDetalle();

                                // Llena la información complementaria de la EDS.
                                detalle.codigoSAPEDS = factura.codigoSAPEDS;
                                detalle.ciudad = dataEds["Ciudad"].ToString();
                                detalle.region = dataEds["Region"].ToString();
                                detalle.proyecto = dataEds["Proyecto"].ToString();
                                detalle.almacen = dataEds["Almacen"].ToString();

                                detalle.codigoSAPProducto = "VE0001";
                                detalle.factorConversion = factura.factorConversion;
                                detalle.noIdentificacion = c.Attribute("NoIdentificacion").Value;
                                detalle.cantidad = double.Parse(c.Attribute("Cantidad").Value);
                                detalle.precioUnitario = double.Parse(c.Attribute("ValorUnitario").Value);
                                detalle.claveUnidad = c.Attribute("ClaveUnidad").Value;
                                detalle.unidadMedida = c.Attribute("Unidad").Value;
                                detalle.numeroTicket = detalle.noIdentificacion.Substring(detalle.noIdentificacion.IndexOf('-') + 1,
                                    (detalle.noIdentificacion.Length - detalle.noIdentificacion.IndexOf('-')) - 1);
                                detalle.permisoCRE = detalle.noIdentificacion.Substring(0, detalle.noIdentificacion.IndexOf('-'));

                                factura.detalles.Add(detalle);
                            }

                            // Client SOAP para conectar a B1IF.
                            SoapClient soapClient = new SoapClient();
                            XNamespace nameSpace = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                            var resultSoap =
                                await soapClient.PostAsync(
                                    new Uri(this.configuration["B1iConfig:UrlCrearSalidaMercancias"]),
                                    SoapVersion.Soap11,
                                    body: XElement.Parse(factura.XmlSAP()));

                            // Lee la respuesta SOAP de B1IF.
                            XDocument doc = XDocument.Parse(resultSoap.Content.ReadAsStringAsync().Result);
                            IEnumerable<XElement> responses = doc.Descendants("response");
                            string result = string.Empty;
                            string actionMessage = string.Empty;
                            foreach (XElement res in responses)
                            {
                                result = (string)res.Element("Result");
                                actionMessage = (string)res.Element("ActionMessage");
                            }

                            // Valida si se creo la Factura.
                            if (result == "success")
                            {
                                responseItem.numeroDocumento = int.Parse(actionMessage);
                                responseItem.uuid = uuid;
                                response.response.Add(responseItem);
                            }
                            // Devuelve el mensaje de error.
                            else
                            {
                                response.errorCode = 4;
                                response.errorMsg += "Error Factura " + uuid + ": " + actionMessage;
                                responseItem.numeroDocumento = 0;
                                responseItem.uuid = uuid;
                                response.response.Add(responseItem);
                                Program.GuardarErrorXml("FTC", factura.XmlSAP());
                            }
                        }
                        // Devuelve el mensaje de error.
                        else
                        {
                            response.errorCode = 1;
                            response.errorMsg += "Error Factura " + uuid + ": " + "Revise la confirguación de la EDS en SAP.";
                            responseItem.numeroDocumento = 0;
                            responseItem.uuid = uuid;
                            response.response.Add(responseItem);
                            Program.GuardarErrorXml("FTC", factura.XmlSAP());
                        }
                    }
                }
                catch (Exception e)
                {
                    response.errorCode = 3;
                    response.errorMsg = e.Message;
                    Program.GuardarErrorXml("FTC", factura.XmlSAP());
                }
            }
            dataBase.Desconectar();
            
            // Devuelve la respuesta.
            return response;
        }

        /// <summary>
        /// Decodifica el string recibido en base 64.
        /// </summary>
        /// <param name="base64EncodedData">String codificada en base 64.</param>
        public string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}

