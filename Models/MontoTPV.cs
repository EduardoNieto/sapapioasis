﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{
    public class MontoTPV
    {
        public string codigoSAP { get; set; }
        public string fechaCierreDia { get; set; }
        public double montoTarjetaCredito { get; set; }
        public double montoTarjetaDebito { get; set; }
    }

    public class MontosTPVResponse
    {
        public int errorCode { get; set; }
        public string errorMsg { get; set; }
    }
}
