﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{
    public class FacturaContado
    {
        public string codigoSAPEDS { get; set; }
        public double factorConversion { get; set; }
        public string xml { get; set; }

        public string codigoSAPCliente { get; set; }
        public string fechaVenta { get; set; }
        public string comentario { get; set; }
        public string referencia { get; set; }
        public string hora { get; set; }
        public string formaPago { get; set; }
        public string medioPago { get; set; }
        public List<FacturaDetalle> detalles { get; set; }

        public double RoundUp(double number, int decimalPlaces)
        {
            double dValue = Convert.ToDouble(Decimal.Round(Convert.ToDecimal(number), decimalPlaces));
            return dValue;
        }

        public string XmlSAP()
        {
            string xml = string.Empty;
            xml = "<BOM><BO><AdmInfo><Object>13</Object><Version>2</Version></AdmInfo>" +
                $@"<Documents><row><DocDate>{this.fechaVenta}</DocDate><Comments>{this.comentario}</Comments>" +
                $@"<CardCode>{this.codigoSAPCliente}</CardCode><U_Referencia>{this.referencia}</U_Referencia>" +
                $@"<PaymentMethod>{this.formaPago}</PaymentMethod><PaymentGroupCode>1</PaymentGroupCode></row></Documents><Document_Lines>";

            foreach (FacturaDetalle detalle in detalles)
            {
                xml += $@"<row><WarehouseCode>{detalle.almacen}</WarehouseCode><CostingCode>{detalle.region}</CostingCode>" +
                    $@"<CostingCode2>{detalle.ciudad}</CostingCode2><CostingCode3>{detalle.codigoSAPEDS}</CostingCode3>" +
                    $@"<ProjectCode>{detalle.proyecto}</ProjectCode>";

                // Agrega el código de artículo.
                xml += "<ItemCode>" + detalle.codigoSAPProducto + "</ItemCode>";
                // Concatena el número de ticket.
                xml += "<U_TCK_TICKET>" + detalle.numeroTicket + "</U_TCK_TICKET>";
                // Concatena fecha en formato AAAAMMDD.
                xml += "<U_TCK_FECHA>" + this.fechaVenta + "</U_TCK_FECHA>";
                // Concatena la hora en formato hhmm.
                xml += "<U_TCK_HORA>" + this.hora + "</U_TCK_HORA>";
                // Concatena el permiso de la CRE.
                xml += "<U_TCK_PERMISO_CRE>" + detalle.permisoCRE + "</U_TCK_PERMISO_CRE>";
                
                // Concatena el tipo de consumo y la cuenta contable.
                xml += "<U_TCK_MEDIO_PAGO>" + this.medioPago + "</U_TCK_MEDIO_PAGO>";

                // Concatena el factor de conversión.
                xml += "<U_TCK_FACTOR_CONV>" + detalle.factorConversion + "</U_TCK_FACTOR_CONV>";
                // Concatena el comentario
                xml += "<U_ISO_COMENTARIO>" + this.comentario + "</U_ISO_COMENTARIO>";
                // Concatena la fecha de envio de Oasis
                xml += "<U_ISO_FECHA>" + this.fechaVenta + "</U_ISO_FECHA>";
                // Concatena el descuento.
                xml += "<DiscountPercent>" + detalle.descuento + "</DiscountPercent>";
                xml += "<U_TCK_PRECIO_UNIT>" + detalle.precioUnitario + "</U_TCK_PRECIO_UNIT>";
                xml += "<U_TCK_CLAVEUNIDAD>" + detalle.claveUnidad + "</U_TCK_CLAVEUNIDAD>";
                // Conctanea el nombre de unidad del SAT.
                xml += "<U_TCK_NOMBREUNIDAD>" + detalle.unidadMedida + "</U_TCK_NOMBREUNIDAD>";
                // Concatena el tipode consumo
                xml += "<U_TCK_TIPO>T</U_TCK_TIPO>";

                // Valida si el artículo es GNV
                if (detalle.codigoSAPProducto == "VE0001")
                {
                    // Agrega la cantidad en litros.
                    xml += "<Quantity>" + (detalle.cantidad / detalle.factorConversion) + "</Quantity>"; //this.RoundUp(detalle.cantidad / detalle.factorConversion, 4) + " </Quantity>";
                    // Agrega el precio en litros.
                    xml += "<UnitPrice>" + (detalle.precioUnitario * detalle.factorConversion) + "</UnitPrice>"; //this.RoundUp(detalle.precioUnitario * detalle.factorConversion, 4) + "</UnitPrice>";
                    // Concatena la cantidad en Metros cúbicos.
                    xml += "<U_TCK_CANTIDAD_MC>" + detalle.cantidad + "</U_TCK_CANTIDAD_MC>";
                    // Concatena el precio en Metro cúbicos.
                    xml += "<U_TCK_PRECIO_MC>" + detalle.precioUnitario + "</U_TCK_PRECIO_MC>";
                    // Concatena la clave de unidad del SAT.
                }
                else
                {
                    // Agrega la cantidad.
                    xml += "<Quantity>" + detalle.cantidad + "</Quantity>";
                    // Agrega el precio en litros.
                    xml += "<UnitPrice>" + detalle.precioUnitario + "</UnitPrice>";
                }

                xml += "</row>";
            }

            xml += "</Document_Lines></BO></BOM>";

            return xml;
        }
    }

    public class FacturaContadoResponse
    {
        public int errorCode { get; set; }
        public string errorMsg { get; set; }
        public List<Response> response { get; set; }
        public struct Response
        {
            public int numeroDocumento { get; set; }
            public string uuid { get; set; }
        }
    }

    public class FacturaDetalle
    {
        /// <summary>
        /// Código SAP del artículo.
        /// </summary>
        public string codigoSAPProducto { get; set; }
        /// <summary>
        /// Cantidad vendida en el ticket.
        /// </summary>
        public double cantidad { get; set; }
        public string claveUnidad { get; set; }
        /// <summary>
        /// Unidad de medida del SAT.
        /// </summary>
        public string unidadMedida { get; set; }
        /// <summary>
        /// Factor utilizado para la conversión de litros a metros cúbicos. 
        /// Sólo aplica para gas natural comprimido.
        /// </summary>
        public double factorConversion { get; set; }
        /// <summary>
        /// Precio unitario del artículo, incluyendo impuestos.
        /// </summary>
        public double precioUnitario { get; set; }
        /// <summary>
        /// Precio de la venta, incluye impuestos y el descuento en caso de que aplique.
        /// </summary>
        public string noIdentificacion { get; set; }
        public double descuento { get; set; }
        public string codigoSAPEDS { get; set; }
        public string region { get; set; }
        public string ciudad { get; set; }
        public string proyecto { get; set; }
        public string almacen { get; set; }
        public string permisoCRE { get; set; }
        public string medioPago { get; set; }
        public string numeroTicket { get; set; }
    }
}

