﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SapApiOasis.Models
{
    public class SalidaMercancia
    {
        public string numeroEnvio { get; set; }
        public string almacenOrigen { get; set; }
        public string responsableUser { get; set; }
        public string codigoSAPEDS { get; set; }

        public Producto[] productos { get; set; }        

        public string FechaSAP(string fechaOrigen)
        {
            string fechaSAP = string.Empty;

            fechaSAP = fechaOrigen.Substring(6, 4) + fechaOrigen.Substring(3, 2) + fechaOrigen.Substring(0, 2);

            return fechaSAP;
        }

        public string XmlSAP() 
        {
            string xml = string.Empty;
            // Añade la información de cabecera del documento
            xml = "<BOM><BO><AdmInfo><Object>60</Object><Version>2</Version></AdmInfo>" +
                $@"<Documents><row><DocDate>{this.FechaSAP(this.productos[0].fechaSalida)}</DocDate>" +
                "<Comments>Salida de mercancía desde OASIS.</Comments>" +
                $@"<U_ISO_NUMENVIO>{this.numeroEnvio}</U_ISO_NUMENVIO><U_ISO_RESPUSER>{this.responsableUser}</U_ISO_RESPUSER>" +
                "</row></Documents><Document_Lines>";
            
            for (int i = 0; i < this.productos.Length; i++)
            {
                // Añade una línea con la cantidad vendida/recibida
                if (this.productos[i].cantidadRecibida > 0)
                {
                    xml += $@"<row><ItemCode>{this.productos[i].codigoSAPProducto}</ItemCode>" +
                        $@"<Quantity>{this.productos[i].cantidadRecibida}</Quantity>" +
                        $@"<WarehouseCode>{this.productos[i].almacen}</WarehouseCode><CostingCode>{this.productos[i].region}</CostingCode>" +
                        $@"<CostingCode2>{this.productos[i].ciudad}</CostingCode2><CostingCode3>{this.codigoSAPEDS}</CostingCode3>" +
                        $@"<U_ISO_INDICADOR>V</U_ISO_INDICADOR><U_ISO_COMENTARIO>{this.productos[i].comentario}</U_ISO_COMENTARIO>" +
                        $@"<U_ISO_FECHA>{this.FechaSAP(this.productos[i].fechaSalida)}</U_ISO_FECHA></row>";
                }

                // Añade una línea con la cantidad devuelta
                if (this.productos[i].cantidadDevolucion > 0)
                {
                    xml += $@"<row><ItemCode>{this.productos[i].codigoSAPProducto}</ItemCode>" +
                        $@"<Quantity>{this.productos[i].cantidadDevolucion}</Quantity>" +
                        $@"<WarehouseCode>{this.productos[i].almacen}</WarehouseCode><CostingCode>{this.productos[i].region}</CostingCode>" +
                        $@"<CostingCode2>{this.productos[i].ciudad}</CostingCode2><CostingCode3>{this.codigoSAPEDS}</CostingCode3>" +
                        $@"<U_ISO_INDICADOR>D</U_ISO_INDICADOR><U_ISO_COMENTARIO>{this.productos[i].comentario}</U_ISO_COMENTARIO>" +
                        $@"<U_ISO_FECHA>{this.FechaSAP(this.productos[i].fechaSalida)}</U_ISO_FECHA></row>";
                }

                // Añade una línea con la cantidad de incidencias
                if (this.productos[i].cantidadIcidencia > 0)
                {
                    xml += $@"<row><ItemCode>{this.productos[i].codigoSAPProducto}</ItemCode>" +
                        $@"<Quantity>{this.productos[i].cantidadIcidencia}</Quantity>" +
                        $@"<WarehouseCode>{this.productos[i].almacen}</WarehouseCode><CostingCode>{this.productos[i].region}</CostingCode>" +
                        $@"<CostingCode2>{this.productos[i].ciudad}</CostingCode2><CostingCode3>{this.codigoSAPEDS}</CostingCode3>" +
                        $@"<U_ISO_INDICADOR>I</U_ISO_INDICADOR><U_ISO_COMENTARIO>{this.productos[i].comentario}</U_ISO_COMENTARIO>" +
                        $@"<U_ISO_FECHA>{this.FechaSAP(this.productos[i].fechaSalida)}</U_ISO_FECHA></row>";
                }
            }

            xml += "</Document_Lines></BO></BOM>";

            return xml;
        }
    }

    public class Producto
    {
        public string codigoSAPProducto { get; set; }
        public decimal cantidadEnvio { get; set; }
        public decimal cantidadRecibida { get; set; }
        public decimal cantidadDevolucion { get; set; }
        public decimal cantidadIcidencia { get; set; }
        public string fechaSalida { get; set; } // "fechaSalida": "DD/MM/AAAA",
        public string comentario { get; set; }
        public string almacen { get; set; }
        public string region { get; set; }
        public string ciudad { get; set; }
    }

    public class SalidaMercanciaResponse
    {
        public int errorCode { get; set; }
        public string errorMsg { get; set; }
        //public Response response { get; set; }

        public struct Response
        {
            public int DocNum { get; set; }
        }
    }
}
