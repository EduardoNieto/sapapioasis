﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{
    public class Recaudo
    {
        public string codigoSAP { get; set; }
        public string fechaCierreDia { get; set; }
        public List<Financiera> financieras { get; set; }
    }

    public class Financiera
    {
        public string nombre { get; set; }
        public double montoRecuado { get; set; }
    }

    public class RecaudoResponse 
    {
        public int errorCode { get; set; }
        public string errorMsg { get; set; }
        public List<Response> response { get; set; }

        public struct Response
        {
            public string nombre { get; set; }
            public string status { get; set; }
        }
    }
}
