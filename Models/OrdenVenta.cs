﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{
    public class OrdenVenta
    {
        public string codigoSAPEDS { get; set; }
        public string codigoSAPCliente { get; set; }        
        public string fechaEnvio { get; set; }
        public string fechaVenta { get; set; }
        public string comentario { get; set; }
        public string nombreCliente { get; set; }
        public string referencia { get; set; }
        public string numAtCard { get; set; }
        public string numeroTicket { get; set; }
        public int kilometraje { get; set; }
        public string placa { get; set; }
        public string horaTerminoVenta { get; set; }
        public string descFlota { get; set; }
        public string descSubFlota { get; set; }
        public List<Detalle> detalle { get; set; }
        public string region { get; set; }
        public string ciudad { get; set; }
        public string proyecto { get; set; }
        public string almacen { get; set; }
        public string permisoCRE { get; set; }
        public string medioPago { get; set; }

        /// <summary>
        /// Convierte la fecha Oasis a fecha SAP.
        /// </summary>
        /// <param name="fechaOrigen">Fecha en formato Oasis dd/mm/yyyy</param>
        /// <returns>Fecha en formato SAP yyyymmdd</returns>
        public string FechaSAP(string fechaOrigen)
        {
            string fechaSAP = string.Empty;

            fechaSAP = fechaOrigen.Substring(6, 4) + fechaOrigen.Substring(3, 2) + fechaOrigen.Substring(0, 2);

            return fechaSAP;
        }

        /// <summary>
        /// Convierte la Hora en formato Oasis a formato SAP.
        /// </summary>
        /// <param name="horaOrigen">Hora en formato Oasis hh:mm:ss</param>
        /// <returns>Hora en formato SAP hhmm</returns>
        public string HoraSAP(string horaOrigen)
        {
            string horaSAP = string.Empty;

            horaSAP = horaOrigen.Substring(0, 2) + horaOrigen.Substring(3, 2);

            return horaSAP;
        }

        /// <summary>
        /// Genera la estructua XML de la Oferta de Venta.
        /// </summary>
        /// <returns>Un string XML.</returns>
        public string XmlSAP()
        {
            string xml = string.Empty;
            xml = "<BOM><BO><AdmInfo><Object>23</Object><Version>2</Version></AdmInfo>" +
                $@"<Documents><row><DocDate>{this.FechaSAP(this.fechaVenta)}</DocDate><Comments>{this.comentario}</Comments>" +
                $@"<CardCode>{this.codigoSAPCliente}</CardCode><NumAtCard>{this.numAtCard}</NumAtCard>" +
                $@"<U_Referencia>{this.referencia}</U_Referencia></row></Documents><Document_Lines>";

            foreach (Detalle detalleItem in detalle)
            {
                xml += $@"<row><WarehouseCode>{this.almacen}</WarehouseCode><CostingCode>{this.region}</CostingCode>" +
                    $@"<CostingCode2>{this.ciudad}</CostingCode2><CostingCode3>{this.codigoSAPEDS}</CostingCode3>" +
                    $@"<ProjectCode>{this.proyecto}</ProjectCode>";

                // Agrega el código de artículo.
                xml += "<ItemCode>" + detalleItem.codigoSAPProducto + "</ItemCode>";
                // Concatena el número de ticket.
                xml += "<U_TCK_TICKET>" + this.numeroTicket + "</U_TCK_TICKET>";
                // Concatena fecha en formato AAAAMMDD.
                xml += "<U_TCK_FECHA>" + FechaSAP(this.fechaVenta) + "</U_TCK_FECHA>";
                // Concatena la hora en formato hhmm.
                xml += "<U_TCK_HORA>" + HoraSAP(this.horaTerminoVenta) + "</U_TCK_HORA>";
                // Concatena el permiso de la CRE.
                xml += "<U_TCK_PERMISO_CRE>" + this.permisoCRE + "</U_TCK_PERMISO_CRE>";
                // Concatena el tipo de consumo y la cuenta contable.
                if (this.comentario.Contains("Credito"))
                {
                    xml += "<U_TCK_TIPO>C</U_TCK_TIPO><AccountCode>4140001002</AccountCode>";
                }
                else if (this.comentario.Contains("Prepago"))
                {
                    xml += "<U_TCK_TIPO>P</U_TCK_TIPO><AccountCode>4140001001</AccountCode>";
                }
                else
                {
                    xml += "<U_TCK_TIPO>T</U_TCK_TIPO><AccountCode>4140001001</AccountCode>";
                    if (this.medioPago == "Efectivo") 
                    {
                        xml += "<U_TCK_MEDIO_PAGO>E</U_TCK_MEDIO_PAGO>";
                    }
                    else if (this.medioPago == "TC")
                    {
                        xml += "<U_TCK_MEDIO_PAGO>C</U_TCK_MEDIO_PAGO>";
                    }
                    else if (this.medioPago == "TD")
                    {
                        xml += "<U_TCK_MEDIO_PAGO>D</U_TCK_MEDIO_PAGO>";
                    }
                    else if (this.medioPago == "Puntos")
                    {
                        xml += "<U_TCK_MEDIO_PAGO>P</U_TCK_MEDIO_PAGO>";
                    }
                }

                // Concatena la placa.
                xml += "<U_TCK_PLACA>" + this.placa + "</U_TCK_PLACA>";
                // Concatena la flota.
                xml += "<U_TCK_FLOTA>" + this.descFlota + "</U_TCK_FLOTA>";
                // Concatena la subflota.
                xml += "<U_TCK_SUBFLOTA>" + this.descSubFlota + "</U_TCK_SUBFLOTA>";
                // Concatena el kilometraje.
                xml += "<U_TCK_KILOMETRAJE>" + this.kilometraje + "</U_TCK_KILOMETRAJE>";
                // Concatena el factor de conversión.
                xml += "<U_TCK_FACTOR_CONV>" + detalleItem.factorConversion + "</U_TCK_FACTOR_CONV>";
                // Concatena el comentario
                xml += "<U_ISO_COMENTARIO>" + this.comentario + "</U_ISO_COMENTARIO>";
                // Concatena la fecha de envio de Oasis
                xml += "<U_ISO_FECHA>" + FechaSAP(this.fechaEnvio) + "</U_ISO_FECHA>";
                // Concatena el descuento.
                xml += "<DiscountPercent>" + detalleItem.descuento + "</DiscountPercent>";
                xml += "<U_TCK_PRECIO_UNIT>" + detalleItem.precioUnitario + "</U_TCK_PRECIO_UNIT>";
                xml += "<U_TCK_PRECIO_VENTA>" + detalleItem.precioVenta + "</U_TCK_PRECIO_VENTA>";

                // Valida si el artículo es GNV
                if (detalleItem.codigoSAPProducto == "VE0001")
                {                   
                    // Agrega la cantidad en litros.
                    xml += "<Quantity>" + detalleItem.cantidad / detalleItem.factorConversion + "</Quantity>";
                    // Agrega el precio en litros.
                    xml += "<UnitPrice>" + (detalleItem.precioUnitario / (1 + Program.IVA)) * detalleItem.factorConversion + "</UnitPrice>";                    
                    // Concatena la cantidad en Metros cúbicos.
                    xml += "<U_TCK_CANTIDAD_MC>" + detalleItem.cantidad + "</U_TCK_CANTIDAD_MC>";
                    // Concatena el precio en Metro cúbicos.
                    xml += "<U_TCK_PRECIO_MC>" + (detalleItem.precioUnitario / (1 + Program.IVA)) + "</U_TCK_PRECIO_MC>";
                    // Concatena la clave de unidad del SAT.
                    xml += "<U_TCK_CLAVEUNIDAD>" + detalleItem.unidadMedida + "</U_TCK_CLAVEUNIDAD>";
                    // Conctanea el nombre de unidad del SAT.
                    xml += "<U_TCK_NOMBREUNIDAD>Metro cubico</U_TCK_NOMBREUNIDAD>";
                }
                else
                {
                    // Agrega la cantidad en litros.
                    xml += "<Quantity>" + detalleItem.cantidad + "</Quantity>";
                    // Agrega el precio en litros.
                    xml += "<UnitPrice>" + (detalleItem.precioUnitario / (1 + Program.IVA)) + "</UnitPrice>";
                    // Conctanea el nombre de unidad del SAT.
                    xml += "<U_TCK_NOMBREUNIDAD>Pieza</U_TCK_NOMBREUNIDAD>";
                }

                xml += "</row>";
            }

            xml += "</Document_Lines></BO></BOM>";

            return xml;
        }
    }

    public class OrdenVentaResponse
    {
        public int errorCode { get; set; }
        public string errorMsg { get; set; }
        public List<Response> response { get; set; }
        public struct Response
        {
            public int folioSAP { get; set; }
            public int numeroDocumento { get; set; }
            public string numeroTicket { get; set; }
        }
    }

    public class Detalle
    {
        /// <summary>
        /// Código SAP del artículo.
        /// </summary>
        public string codigoSAPProducto { get; set; }
        /// <summary>
        /// Cantidad vendida en el ticket.
        /// </summary>
        public double cantidad { get; set; }
        /// <summary>
        /// Unidad de medida del SAT.
        /// </summary>
        public string unidadMedida { get; set; }
        /// <summary>
        /// Factor utilizado para la conversión de litros a metros cúbicos. 
        /// Sólo aplica para gas natural comprimido.
        /// </summary>
        public double factorConversion { get; set; }
        /// <summary>
        /// Precio unitario del artículo, incluyendo impuestos.
        /// </summary>
        public double precioUnitario { get; set; }
        /// <summary>
        /// Precio de la venta, incluye impuestos y el descuento en caso de que aplique.
        /// </summary>
        public double precioVenta { get; set; }
        /// <summary>
        /// Porcentaje de descuento aplicado al artículo, en caso de que exista.
        /// </summary>
        public double descuento { get; set; }
    }
}
