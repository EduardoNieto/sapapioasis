﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{
    public class SocioDeNegocios
    {
        public string errorCode { get; set; }

        public string errorMsg { get; set; }

        public SocioDeNegocios()
        {

        }

        public SocioDeNegociosResponse response { get; set; }

        public SocioDeNegocios(string errorCode, string errorMsg, SocioDeNegociosResponse response)
        {
            this.errorCode = errorCode;
            this.errorMsg = errorMsg;
            this.response = response;
        }
    }

    public class SocioDeNegociosResponse
    {
        public string existeCliente { get; set; }
        public double limiteCredito { get; set; }        

    }
}
