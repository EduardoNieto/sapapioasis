﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{
    public class Articulo
    {
        /// <summary>
        /// Código de error.
        /// </summary>
        public string errorCode { get; set; }
        /// <summary>
        /// Mensaje de error.
        /// </summary>
        public string errorMsg { get; set; }
        /// <summary>
        /// Respuesta con los datos del artículo.
        /// </summary>
        public ArticuloResponse response { get; set; }

        /// <summary>
        /// Constructor de la clase sin parámetros.
        /// </summary>
        /// 
        public Articulo()
        {
        }

        /// <summary>
        /// Constructor de la clase.
        /// </summary>
        /// <param name="errorCode">Código de error.</param>
        /// <param name="errorMsg">Mensaje de error.</param>
        /// <param name="response">Datos del artículo.</param>
        public Articulo(string errorCode, string errorMsg, ArticuloResponse response)
        {
            this.errorCode = errorCode;
            this.errorMsg = errorMsg;
            this.response = response;
        }
    }

    public class ArticuloResponse
    {
        /// <summary>
        /// Descripción del artículo.
        /// </summary>
        public string descripcion { get; set; }
        /// <summary>
        /// Factor del impuesto IVA.
        /// </summary>
        public int iva { get; set; }
        /// <summary>
        /// Código de unidad de medida.
        /// </summary>
        public string codigoUnidadMedida { get; set; }
        /// <summary>
        /// Unidad de medida.
        /// </summary>
        public string unidadMedida { get; set; } 
        /// <summary>
        /// Stock mínimo en el inventario.
        /// </summary>
        public double stockMinimo { get; set; }
        /// <summary>
        /// Precio del artículo.
        /// </summary>
        public double precio { get; set; }
    }
}