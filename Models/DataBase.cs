﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace SapApiOasis.Models
{
    public class DataBase
    {
        private SqlConnection oSqlConnection;

        public bool Conectar(string connectionString)
        {
            bool bConectado = false;

            try
            {
                this.oSqlConnection = new SqlConnection(connectionString);
                this.oSqlConnection.Open();
                bConectado = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }

            return bConectado;
        }

        public bool Conectado()
        {
            bool bConectado = false;

            if (this.oSqlConnection.State == System.Data.ConnectionState.Open)
            {
                bConectado = true;
            }

            return bConectado;
        }

        public void Desconectar()
        {
            try
            {
                if (this.Conectado())
                {
                    this.oSqlConnection.Close();
                    this.oSqlConnection.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public DataSet ValidarCodigoEstacion(string codigo_eds, out string error)
        {
            error = string.Empty;
            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDataAdapter = null;

            string sQuery = @$"EXEC [dbo].[ISO_GET_ValidarCodigoSAPEstacion] '{codigo_eds}'";

            try
            {
                if (this.Conectado())
                {
                    oDataAdapter = new SqlDataAdapter(sQuery, this.oSqlConnection);
                    oDataAdapter.Fill(oDataSet);
                }
            }
            catch (Exception e)
            {
                error = e.Message;
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (oDataAdapter != null)
                {
                    oDataAdapter.Dispose();
                }
            }

            return oDataSet;
        }

        public void ValidaSalidaMercancia(string codigoSAPEDS, ref Models.Producto producto, out string error)
        {
            error = string.Empty;

            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDataAdapter = null;

            string sQuery = @$"EXEC [dbo].[ISO_POST_ValidarSalidaMercancias] '{codigoSAPEDS}', '{producto.codigoSAPProducto}', {producto.cantidadEnvio}";
            //EXEC [dbo].[ISO_POST_ValidarSalidaMercancias] 'OBRERA' '32321' 2
            try
            {
                if (this.Conectado())
                {
                    oDataAdapter = new SqlDataAdapter(sQuery, this.oSqlConnection);
                    oDataAdapter.Fill(oDataSet);
                    if (oDataSet.Tables[0].Rows.Count > 0)
                    {
                        error = oDataSet.Tables[0].Rows[0].Field<string>("Error");
                        producto.almacen = oDataSet.Tables[0].Rows[0].Field<string>("Almacen");
                        producto.region = oDataSet.Tables[0].Rows[0].Field<string>("Region");
                        producto.ciudad = oDataSet.Tables[0].Rows[0].Field<string>("Ciudad");
                    }
                }
            }
            catch (Exception e)
            {
                error = e.Message;
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (oDataAdapter != null)
                {
                    oDataAdapter.Dispose();
                }
            }
        }

        public DataSet ValidarSocioDeNegocios(string codigo_sn, out string error)
        {
            error = string.Empty;
            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDataAdapter = null;

            string sQuery = @$"EXEC [dbo].[ISO_GET_ValidarSocioDeNegocios] '{codigo_sn}'";

            try
            {
                if (this.Conectado())
                {
                    oDataAdapter = new SqlDataAdapter(sQuery, this.oSqlConnection);
                    oDataAdapter.Fill(oDataSet);
                }
            }
            catch (Exception e)
            {
                error = e.Message;
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (oDataAdapter != null)
                {
                    oDataAdapter.Dispose();
                }
            }

            return oDataSet;
        }

        public async Task<object> ValidarArticulo(string codigo_articulo)
        {
            string error = string.Empty;
            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDataAdapter = null;

            string sQuery = @$"EXEC [dbo].[ISO_GET_ValidarArticulo] '{codigo_articulo}'";

            try
            {
                if (this.Conectado())
                {
                    oDataAdapter = new SqlDataAdapter(sQuery, this.oSqlConnection);
                    await Task.Run(() => oDataAdapter.Fill(oDataSet));

                    //await Task.Run(() => oDataAdapter.Fill(oDataSet)).ContinueWith((t) =>
                    //{
                    //    if (t.IsFaulted) throw t.Exception;
                    //    //if (t.IsCompleted) //optionally do some work);
                    //});
                }
            }
            catch (Exception e)
            {
                error = e.Message;
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (oDataAdapter != null)
                {
                    oDataAdapter.Dispose();
                }

            }

            if (error != string.Empty)
                return error;
            else
                return oDataSet;
        }

        /// <summary>
        /// Consulta en la base de datos los datos de la EDS para crear la Oferta de VEnta.
        /// </summary>
        /// <param name="codigoEstacion">Código de la EDS.</param>
        /// <returns>Un string con el error o un DataSet con los datos de la EDS.</returns>
        public async Task<object> ValidarOrdenVenta(string codigoEstacion)
        {
            string error = string.Empty;
            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDataAdapter = null;

            string sQuery = @$"EXEC [dbo].[ISO_POST_ValidarOrdenVenta] '{codigoEstacion}'";

            try
            {
                if (this.Conectado())
                {
                    oDataAdapter = new SqlDataAdapter(sQuery, this.oSqlConnection);
                    await Task.Run(() => oDataAdapter.Fill(oDataSet));
                    error = oDataSet.Tables[0].Rows[0].Field<string>("Error");
                }
            }
            catch (Exception e)
            {
                error = e.Message;
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (oDataAdapter != null)
                {
                    oDataAdapter.Dispose();
                }
            }

            if (error != string.Empty && error != null)
                return error;
            else
                return oDataSet;
        }
        public void ValidaConfirmacionEnvio(string numeroEnvio, ref Models.Producto producto, out string error)
        {
            error = string.Empty;

            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDataAdapter = null;

            string sQuery = @$"EXEC [dbo].[ISO_POST_ValidarConfirmacion] {numeroEnvio}, '{producto.codigoSAPProducto}', {producto.cantidadEnvio}";

            try
            {
                if (this.Conectado())
                {
                    oDataAdapter = new SqlDataAdapter(sQuery, this.oSqlConnection);
                    oDataAdapter.Fill(oDataSet);
                    if (oDataSet.Tables[0].Rows.Count > 0)
                    {
                        error = oDataSet.Tables[0].Rows[0].Field<string>("Error");
                    }
                }
            }
            catch (Exception e)
            {
                error = e.Message;
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (oDataAdapter != null)
                {
                    oDataAdapter.Dispose();
                }
            }
        }

    }
}
