﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SapApiOasis.Models
{
    public class Estacion
    {
        public string errorCode { get; set; }

        public string errorMsg { get; set; }

        public EstacionResponse response { get; set; }

        public Estacion()
        {
        }

        public Estacion(string errorCode, string errorMsg, EstacionResponse response)
        {
            this.errorCode = errorCode;
            this.errorMsg = errorMsg;
            this.response = response;
        }
    }

    public class EstacionResponse
    {
        /// <summary>
        /// Nombre de la Estación de Servicio
        /// </summary>
        public string nombreEDS { get; set; }

        public string zona { get; set; }

        public string plaza { get; set; }

        public string calle { get; set; }

        public string numero { get; set; }

        public string colonia { get; set; }

        public string ciudad { get; set; }

        public string codigoPostal { get; set; }

        public EstacionResponse()
        {
        }

        public EstacionResponse(string nombreEDS, string zona, string plaza, string calle, 
            string numero, string colonia, string ciudad, string codigoPostal)
        {
            this.nombreEDS = nombreEDS;
            this.zona = zona;
            this.plaza = plaza;
            this.calle = calle;
            this.numero = numero;
            this.colonia = colonia;
            this.ciudad = ciudad;
            this.codigoPostal = codigoPostal;
        }
    }
}
